import 'package:flutter/material.dart';
import 'package:fluttercallback/main.dart';

class ActionButton extends StatelessWidget {
  final FormType state;
  final VoidCallback onPressed;

  ActionButton({this.state, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        textColor: Colors.white,
        color: Colors.blueAccent,
        child: Text(state == FormType.Login ? "Login" : "Register"),
        onPressed: onPressed);
  }
}
