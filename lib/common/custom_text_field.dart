import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController controller;
  final String labelText;
  final bool isObscureText;
  final Function(String) onChanged;
  CustomTextField(
      {this.controller,
      this.labelText,
      this.isObscureText = false,
      this.onChanged});

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(labelText: labelText),
      obscureText: isObscureText,
      onChanged: (text) {
        onChanged(text);
      },
    );
  }
}
