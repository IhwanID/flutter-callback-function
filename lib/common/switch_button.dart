import 'package:flutter/material.dart';
import 'package:fluttercallback/main.dart';

class SwitchButton extends StatelessWidget {
  final FormType state;
  final VoidCallback onPressed;
  SwitchButton({this.state, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(state == FormType.Register
          ? 'Have an account? Click here to login.'
          : 'Don\'nt have an account? Create new Accoun'),
      onPressed: onPressed,
    );
  }
}
