import 'package:flutter/material.dart';
import 'package:fluttercallback/common/action_button.dart';
import 'package:fluttercallback/common/custom_text_field.dart';
import 'package:fluttercallback/common/switch_button.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginRegisterPage(),
    );
  }
}

enum FormType { Login, Register }

class LoginRegisterPage extends StatefulWidget {
  @override
  _LoginRegisterPageState createState() => _LoginRegisterPageState();
}

class _LoginRegisterPageState extends State<LoginRegisterPage> {
  FormType currentState = FormType.Login;
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController usernameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    bool isLogin = currentState == FormType.Login;
    return Scaffold(
      appBar: AppBar(
        title: Text(isLogin ? "Login" : "Register"),
      ),
      body: Container(
        margin: EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            CustomTextField(
              controller: usernameController,
              labelText: 'Username',
              onChanged: (text) {
                print(text);
              },
            ),
            !isLogin
                ? CustomTextField(
                    controller: emailController,
                    labelText: 'Email',
                  )
                : Container(),
            CustomTextField(
              controller: passwordController,
              labelText: 'Password',
              isObscureText: true,
            ),
            SizedBox(
              height: 20,
            ),
            ActionButton(
                state: currentState,
                onPressed: () {
                  if (isLogin) {
                    print(
                        "you are login with id ${usernameController.text} & password ${passwordController.text}");
                  } else {
                    print(
                        "you are register with id ${usernameController.text} , email ${emailController.text} & password ${passwordController.text}");
                  }
                }),
            SwitchButton(
              state: currentState,
              onPressed: () {
                setState(() {
                  if (isLogin) {
                    currentState = FormType.Register;
                  } else {
                    currentState = FormType.Login;
                  }
                });
              },
            )
          ],
        ),
      ),
    );
  }
}
